@ECHO Off
SET dir=%cd%

SET source=%dir%\target\site\serenity

SET datetimef=%date:~-4%_%date:~3,2%_%date:~0,2%_%time:~0,2%_%time:~3,2%

SET dest=%dir%\Reports\%datetimef%

::this acts as a metafilter, so provide the parameter what you
::have used in your '.story' files
SET TEST_CASE="+TestCaseName"

md %dest%
xcopy %source% %dest% /S

echo serenity reports are copied into Reports folder

mvn test -Dmetafilter=%TEST_CASE%


