package com.bdd.TestSteps;

import java.io.File;
import java.io.IOException;

import org.jbehave.core.annotations.*;
import org.openqa.selenium.By;

import com.bdd.Util.Util;

import net.serenitybdd.core.pages.PageObject;

/**
 * A step class which is mapped to the story line
 * 
 * @author Manvendra
 *
 */
public class TestSteps1 extends PageObject {

	@Given("the user is on homepage")
	public void test() throws IOException {
		getDriver().navigate().to("https://www.google.com");
	}

	@Then("user does something")
	public void doS() {
		getDriver().findElement(By.name("q")).sendKeys(Util.generateRandomPhoneNumber("91"));
		getDriver().navigate().refresh();
		getDriver().findElement(By.name("q")).sendKeys(Util.generateRandomString(34));
		Util.pause(2);
	}
}
