package com.bdd.Acceptance;

import java.io.IOException;

import com.bdd.Util.Constants;
import com.bdd.Util.Util;

import net.serenitybdd.jbehave.SerenityStories;

public class AcceptanceTest extends SerenityStories implements Constants {
	public AcceptanceTest() throws IOException {
		Util.loadPropertyFiles();

		System.setProperty("webdriver.chrome.driver", DIR + "\\src\\test\\resources\\Drivers\\chromedriver.exe");
		System.setProperty("webdriver.gecko.driver", DIR + "\\src\\test\\resources\\Drivers\\geckodriver.exe");
		System.setProperty("webdriver.ie.driver", DIR + "\\src\\test\\resources\\Drivers\\IEDriverServer.exe");
		
		findStoriesCalled("*.story");
		
	}

}
