package com.bdd.Util;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;

import com.bdd.Pages.TestPage1;

/**
 * A set of {@link Constants}
 * 
 * @author Manvendra
 *
 */
public interface Constants {

	String DIR = System.getProperty("user.dir");
	String FILE_NAME = DIR + "\\TestData.xls";

	String ALPHA = "abcdefghijklmnopqrstuvwxyz";
	String NUM = "1234567890";

	Logger UTIL_LOGGER = LogManager.getLogger(Util.class.getName());
	Logger TEST_Page1_LOGGER = LogManager.getLogger(TestPage1.class.getName());
}
