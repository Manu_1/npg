package com.bdd.Util;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A set of common reusable methods
 * 
 * @author Manvendra
 *
 */
public class Util implements Constants {
	public static Properties RESOURCE_FILE_1;

	public static String parentWindow;
	public static Object[] tabs;

	/**
	 * Loads the resource i.e. properties files
	 * 
	 * @throws IOException
	 */
	public static void loadPropertyFiles() throws IOException {
		File f1 = new File(DIR + "\\src\\main\\resources\\Resources\\xpath.properties");
		FileInputStream fis1 = new FileInputStream(f1);
		RESOURCE_FILE_1 = new Properties();
		RESOURCE_FILE_1.load(fis1);

		System.out.println("Property file(s) successfully loaded!!");

	}

	/**
	 * Reads the corresponding cell value(s) of an excel sheet based on its column
	 * values
	 * 
	 * @param fileName
	 * @param sheetName
	 * @param columnValue
	 * @return the respective cell value(s)
	 * @throws IOException
	 */
	public static String readExcel(String fileName, String sheetName, String columnValue) throws IOException {
		File f = new File(fileName);
		// creates an input stream of the respective file
		FileInputStream fis = new FileInputStream(f);

		// instantiate HSSFWorkbook class with input stream parameter
		Workbook w = new HSSFWorkbook(fis);

		// finds the sheet in the workbook with its provided name
		Sheet s = w.getSheet(sheetName);

		// returns the total number of rows populated in the sheet
		int totalRows = (s.getLastRowNum() - s.getFirstRowNum()) + 1;

		// iterates over the rows to find the matching row and returns its corresponding
		// cell value
		for (int i = 0; i < totalRows; i++) {
			Row r = s.getRow(i);
			Cell cell = r.getCell(i);

			if (cell.getStringCellValue().equals(columnValue)) {
				ArrayList<String> list = new ArrayList<>();
				for (int j = 1; j < r.getLastCellNum(); j++) {
					list.add(r.getCell(j).getStringCellValue());
				}
				return list.toString();

			}
		}

		return null;
	}

	/**
	 * Write into an excel sheet based on below Params.
	 * 
	 * @param fileName
	 * @param sheetName
	 * @param dataToWrite
	 * @throws IOException
	 */

	public static void writeIntoExcel(String fileName, String sheetName, Hashtable<String, Object[]> dataToWrite)
			throws IOException {

		// creates a workbook
		Workbook w = new HSSFWorkbook();

		// creates a sheet of the given name
		HSSFSheet s = (HSSFSheet) w.createSheet(sheetName);

		// create a row object
		HSSFRow row;

		// stores all the keys into Set<String>
		Set<String> keys = dataToWrite.keySet();
		int rownum = 0;

		// iterates over each keys for their respective values
		for (String string : keys) {

			row = s.createRow(rownum++);
			Object[] cellValuesPerKey = dataToWrite.get(string);
			int cellnum = 0;
			// key row
			Cell cell = row.createCell(cellnum++);
			cell.setCellValue(string);

			for (Object obj : cellValuesPerKey) {
				// values of the respective row(which is Key here)
				cell = row.createCell(cellnum++);
				cell.setCellValue((String) obj);
			}

		}

		// creates an output stream
		FileOutputStream fos = new FileOutputStream(new File(fileName));
		// writes that stream to the workbook
		w.write(fos);

		// close the stream
		fos.close();

	}

	/**
	 * Click on a web-element using JavaScript, takes web-element and driver as
	 * parameter
	 * 
	 * @param driver
	 * @param element
	 */
	public static void javaScriptClick(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);

	}

	/**
	 * Click on a web-element using JavaScript, takes driver and x-path as params
	 * 
	 * @param driver
	 * @param xpath
	 */
	public static void javaScriptClick(WebDriver driver, String xpath) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", driver.findElement(By.xpath(xpath)));
	}

	/**
	 * Pause the script for requested seconds
	 * 
	 * @param seconds
	 */
	public static void pause(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns current date in a requested format (e.g. yyyy-MM-dd), using classes->
	 * {@code DateTimeFormatter} and {@code LocalDateTime}
	 * 
	 * @return String of current date
	 */
	public static String currentDateTime(String format) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now).toString();
	}

	/**
	 * This method creates a list of elements for the given x-path and compare its
	 * size. If the list is empty then element is not present in DOM and if its
	 * greater than 0 the element is present in DOM
	 * 
	 * @param driver
	 * @param xpath
	 * @return true if element exists, false otherwise
	 */
	public static boolean isElementExists(WebDriver driver, String xpath) {
		return driver.findElements(By.xpath(xpath)).size() > 0;
	}

	/**
	 * Opens a new tab, stores parent window ID and all the IDs of opened tabs of
	 * the current browser
	 * 
	 * @param driver
	 * @param tabNumToSwitch
	 */
	public static void openNewTab(WebDriver driver) {
		try {
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_T);

			r.keyRelease(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_T);
			Util.pause(2);
			parentWindow = driver.getWindowHandle();

			Set<String> t = driver.getWindowHandles();

			tabs = t.toArray();

		} catch (AWTException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Switches to newly opened tabs of the current browser
	 * 
	 * @param driver
	 * @param tabNumToSwitch
	 */
	public static void switchToNewWindow(WebDriver driver, int tabNumToSwitch) {
		driver.switchTo().window((String) tabs[tabNumToSwitch]);
	}

	/**
	 * Switches back to the parent tab/window of the current browser
	 * 
	 * @param driver
	 */
	public static void switchToParentWindow(WebDriver driver) {
		driver.switchTo().window(parentWindow);
	}

	/**
	 * Generates a random string of length 10
	 * 
	 * @return A random string having length 10
	 */
	static public String generateRandomString() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < 10; i++) {
			Random r = new Random();
			int a = r.nextInt(25);
			char c = ALPHA.charAt(a);
			sb.append(c);
		}
		return sb.toString();

	}

	/**
	 * Generates a random string of the given length
	 * 
	 * @param length
	 * @return Random string of the given length
	 */
	static public String generateRandomString(int length) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < length; i++) {
			Random r = new Random();
			int a = r.nextInt(25);
			if (i % 10 == 0) {
				sb.append(" ");
			}
			char c = ALPHA.charAt(a);
			sb.append(c);
		}
		return sb.toString();

	}

	/**
	 * Generates an email address, having 16 characters
	 * 
	 * @return A randomly generated E-mail address
	 */
	public static String generateRandomEmailAddress() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < 7; i++) {
			Random r = new Random();
			int a = r.nextInt(25);
			char c = ALPHA.charAt(a);
			sb.append(c);
		}
		return sb.toString() + "@" + generateRandomString(4) + "." + generateRandomString(3);
	}

	/**
	 * Generates a phone number for a given country(code). Do not include '+' sign.
	 * 
	 * @param countryCode
	 * @return A country specific phone number
	 */

	public static String generateRandomPhoneNumber(String countryCode) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 10; i++) {
			Random r = new Random();
			int a = r.nextInt(9);
			char c = NUM.charAt(a);
			sb.append(c);
		}
		return "+" + countryCode + sb.toString();
	}

	/**
	 * Sort the given map in ascending order based on its values
	 * 
	 * @param map the map to be sorted
	 * @return the sorted map according to it's values
	 */
	public static HashMap<String, Integer> sortMapByValue(Map<String, Integer> map) {
		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(map.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});

		HashMap<String, Integer> h = new LinkedHashMap<>();
		for (Map.Entry<String, Integer> entry : list) {
			h.put(entry.getKey(), entry.getValue());
		}

		return h;

	}

	/**
	 * Compare the contents of two '.txt' files, line by line. If mismatch occurs,
	 * it will log the line number into the log file along with the mismatched
	 * strings and then return false.
	 * 
	 * @param fileOne
	 * @param fileTwo
	 * @return true if no mismatch occurs, false otherwise
	 * @throws IOException
	 */

	@SuppressWarnings("resource")
	public static boolean compareTwoFileContents(File fileOne, File fileTwo) throws IOException {

		BufferedReader bf1 = new BufferedReader(new FileReader(fileOne));
		StringBuilder sb1 = new StringBuilder();

		BufferedReader bf2 = new BufferedReader(new FileReader(fileTwo));
		StringBuilder sb2 = new StringBuilder();

		String line = null;

		while ((line = bf1.readLine()) != null)
			sb1.append(line + "\n");
		sb1.deleteCharAt(sb1.length() - 1);
		line = null;
		while ((line = bf2.readLine()) != null)
			sb2.append(line + "\n");
		sb2.deleteCharAt(sb2.length() - 1);

		String[] a1 = sb1.toString().split("\n");
		String[] a2 = sb2.toString().split("\n");

		boolean isPassed = true;

		UTIL_LOGGER.info("Comparison started!!!");

		if (a1.length >= a2.length) {
			for (int i = 0; i < a1.length; i++) {
				if (!(a1[i].equalsIgnoreCase(a2[i]))) {
					int l = i + 1;
					UTIL_LOGGER.error("Mismatch occured at line number -> '" + l + "'");
					UTIL_LOGGER.info("File:1 contains '" + a1[i] + "'");
					UTIL_LOGGER.info("File:2 contains '" + a2[i] + "'");
					isPassed = false;
				}
			}
		} else {
			for (int i = 0; i < a2.length; i++) {
				if ((!a2[i].equalsIgnoreCase(a1[i]))) {
					int l = i + 1;
					UTIL_LOGGER.error("Mismatch occured at line number -> '" + l + "'");
					UTIL_LOGGER.info("File:1 contains '" + a1[i] + "'");
					UTIL_LOGGER.info("File:2 contains '" + a2[i] + "'");
					isPassed = false;
				}
			}
		}
		if (isPassed)
			UTIL_LOGGER.info("File content comparison successfully finished with no mismatch!!!");

		else
			UTIL_LOGGER.info("File content comparison finished with errors!!!");

		return isPassed;
	}

	/**
	 * Simply copies the given string into the file of given path using
	 * {@code FileWriter} class.
	 * 
	 * @param s
	 * @param f
	 * @throws IOException
	 */
	public static void copyStringIntoATxtFile(String s, File f) throws IOException {
		FileWriter fw = new FileWriter(f, true);
		s = s + " ";
		fw.write(s);
		fw.close();
	}

	/**
	 * Use to throw custom exception. You can pass your choice of exception as a
	 * parameter and it will throw that exception at runtime but it will not
	 * terminate the program.
	 * 
	 * @param exception message
	 */
	public static void throwCustomException(String exception) {
		@SuppressWarnings("serial")
		class MyException extends Exception {
			public MyException(String s) {
				super(s);
			}
		}
		try {
			throw new MyException(exception);
		} catch (MyException e) {
			UTIL_LOGGER.error("Your custom exception has occured. Exception-> '" + e.getMessage() + "'");
			e.printStackTrace();
		}
	}

	/**
	 * This method will handle {@code StaleElementReferenceException} and explicitly
	 * wait for 10 seconds for the element of the given x-path to be clickable
	 * before throwing an exception.
	 * 
	 * @param driver
	 * @param xpath
	 */
	public static void handleStaleElementException(WebDriver driver, String xpath) {
		new WebDriverWait(driver, 10).ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(xpath))));
	}

}
